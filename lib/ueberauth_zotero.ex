defmodule UeberauthZotero do
  @moduledoc """
  Documentation for `UeberauthZotero`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> UeberauthZotero.hello()
      :world

  """
  def hello do
    :world
  end
end
